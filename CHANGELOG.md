
## [0.6.1] - 2023-03-16

### Fixed
- Minor change to GP5W header row on export to match logger exports

## [0.6.0] - 2023-03-16

### Added

- Support added for opening files from RBR readers (requires `tsp>=1.5.0`)

### Changed
- Information about source file types is now kept in a separate file (`Filetype.py`)
- File open dialogues now use source-specific file filters (e.g `.txt .csv` for GTNP files and `.rsk .xslx, .xls` for RBR files)

### Fixed
- Checking for duplicate dates now considers different depths separately.
