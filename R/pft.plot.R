#=========================================================================================#
##################################### pft.plot ############################################
#=========================================================================================#
# Plots a table with time series data in an interactive plot
# Table must have the following format: time | m<depth in metres> | m<depth in metres> ... 

pft.plot <- function(table, location, depths=NULL) {
  
  require(dygraphs)
  require(xts)
  
  #### Internal functions ######
  
  # Create time series
  f.timeseries <- function(obs) {
    # Get dataframe of unique times
    date <- data.frame(time=unique(obs$time))
    # Get list of unique depths
    depths <- unique(obs$depth)
    # Create empty time series and names
    series <- NULL
    snames <- NULL
    # Create time series for every depth
    for (d in 1:length(depths)) {
      # match the values to times with merge to get regular series
      y <- subset(obs, depth == depths[d], select=c(temp, time))
      m <- merge(x = date, y = y, by = "time", all.x = TRUE)
      #only take columns with values
      if (sum(is.na(m$temp) == 0) > 0) {
        series <- cbind(series, m$temp)
        snames <- c(snames, paste(depths[d], "m"))	
      } 
    }  
    
    # Create time series object
    qxts <- xts(series, order.by=date$time)
    timeseries <- list(qxts, snames)
    return(timeseries)
  }
  
  # Reshape dataframe into long format
  f.long <- function(table){
    # Create list of depths
    f.depths <- function(d) {
      ds <- NULL
      for (i in d) {
        depth <- i
        depth <- gsub("p", "", depth)
        depth <- gsub("m", "-", depth) 
        ds <- append(ds, as.numeric(depth))
      }
      return(ds)
    }
    # Reshape
    obs <- reshape(table, direction = "long", 
                   varying = names(table[2:length(names(table))]), 
                   v.names = "temp", idvar = "time", timevar = "depth",
                   times = f.depths(names(table[2:length(names(table))]))
    )
    row.names(obs) <- seq(1, length(obs$time))
    return(obs)
  }
  
  #### Subset table to depth or depths ####
  if (!is.null(depths)){
    table <- table[, names(table) == "time" | names(table) %in% depths]
  }

  #### Reshape table ####
  
  # Put into long format
  print("Reshaping table")
  obs <- f.long(table)
  
  #### Create time series ####
  print("Create time series")
  timeseries <- f.timeseries(obs)
  # get number of time series
  snamesLen <- length(timeseries[[2]])
  
  #### Plotting time series ####
  print("Plotting time series")
  
  # plot time series
  graph <- dygraph(timeseries[[1]], main=paste(location, collapse=", "), ylab = "Temperature [?C]")
  
  # iterate to create series labels
  for (seriesNum in 1:snamesLen)
  {
    graph <- graph %>% dySeries(paste0("V",seriesNum), label = timeseries[[2]][seriesNum])
  }
  
  graph <- graph %>%
    dyLegend(width = 400) %>%
    dyOptions(labelsUTC = TRUE) %>%
    dyHighlight(highlightSeriesOpts = list(strokeWidth = 2))
  
  # display graph
  graph 
  
}


