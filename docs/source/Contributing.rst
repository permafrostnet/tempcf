======================
Contributing to tempcf
======================

This software is intended to be supported by the community. If you are interested in adding a filter function, we ask that you keep in mind the following recommendations:

1. Filters must return numpy arrays of either `bool` (True/False) or float (real numbers) type. If a filter returns `float` data, it must be between 0 and 1. 

2. The function must accept one or more of the following positional arguments: {values, timestamps, depths}. (e.g. ``new_filter(values, par_1=5`` is correct, but ``new_filter(data, par_1=5)`` is wrong)

3. All other function arguments must have default values. (e.g. ``new_filter(values, p1=None, p2=3)``)

4. Function arguments that have default values should have `type hints <https://docs.python.org/3/library/typing.html>`_. (e.g. ``new_filter(values, p1: str=None, p2: int=3, p3: float=3.1415)``)

5. Filters should be documented using `numpy-style docstrings <https://numpydoc.readthedocs.io/en/latest/format.html#id8>`_. 

This ensures that your contribution will fit into the GUI tool for others to enjoy.

