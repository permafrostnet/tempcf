============
Using tempcf
============

Opening a file
==============

From the File menu, select one of the options under `Open Data Source`. Only some file formats are currently available:

1. NTGS: CSV data in the format specified by the Northwest Territories Geological Survey ground temperature file template.
2. Geoprecision GP5W: CSV exports for older geoprecision loggers
3. Geoprecision FG2: CSV exports for newer geoprecision dataloggers
4. RBR: Data exports from varous RBR loggers

Selecting data
==============
Once data are loaded, you can select data points by drawing on the graph canvas using the left mouse button. Selecting
data will darken the data points slightly.

.. image:: images/select_points.png
   :width: 400

Using the scroll wheel while the mouse is over the graph allows you to zoom in and out on the position of the mouse.
Clicking and dragging using the right mouse button will allow you to pan within the graph. You may also use the
navigation buttons to zoom and pan. If you select one of the navigation buttons (e.g. zoom or pan), you will temporarily
be unable to select data. To re-enable the data selection tool, un-select the active navigation button. Some versions of
the software won't change the look of the navigation buttons when they are selected. In this case, you can use the shape
of the mouse icon to determine which navigation button is selected.

.. image:: images/pan_zoom_select.png
   :width: 300



Editing data
============

Once data are selected, you are able to perform actions on the selected data.  You may replace it with a desired value, or delete it (replaces the data with N/A values).

.. image:: images/action_menu.png
   :width: 300

Turning certain depths on and off
=================================

If you want to restrict selection or modification to one or more depths, you can toggle the ones you want to exclude off.

.. image:: images/toggle_depths.png
   :width: 800

Data from depths that are toggled off cannot be selected, deleted or replaced. Turning off depth will un-select any currently selected data at that depth. Note however that data from depths that are toggled off will still be identified in the filters.

Flagging suspicious data
========================

To take advantage of the library of functions for identifying suspicious data, use the `Filters` submenu to pick a function you are interested in. This will bring up a new menu with help text describing the filter and boxes to input parameters for that filter. Once you are satisfied with your parameters, click OK.

.. image:: images/configure_filter.png
   :width: 500

You have now created a filter. To apply it, select the name of the filter in the list on the right to highlight it and click "apply filter". All data flagged by this filter will change colour. 

.. image:: images/filter_menu.png
   :width: 800

Once data are flagged, you may select all flagged data using the "Select Filtered Data" button. This selects all data points that are visible and flagged by the filter. Note that if a particular depth is turned off (not visible), it will still be included in the filter results. However, any selection, deletion or replacement will not apply to hidden depths. 

Exporting data
==============

Once you have finished your editing, you can save your cleaned data in one of several formats. These options are accessed through the File > Export Changes menu.

tempcf log files 
----------------
When you export your changes, a log file is created that contains information about what changes were made, as well as information about the original file and the version of tempcf that you used. This is whenever you export 

Keyboard shortcuts
==================
The following keyboard shortcuts are available:

* Ctrl+Q: Quit the program
* Ctrl+W: Close the current file
* [1-9]: Toggle the visibility of the corresponding n\ :sup:`th`  depth
* Ctrl+[1-9]: Toggle the visibility of the (n+9)\ :sup:`th` depth 
* ` (backtick): Toggle the visibility of all depths to 'off'
* Ctrl+` (backtick): Toggle the visibility of all depths to 'on'

Bugs
====

Any problems with the code should be added to the issue tracking list on the `GitLab code repository <https://gitlab.com/permafrostnet/permafrost-tempcf/-/issues>`_.





