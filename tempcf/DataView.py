import tkinter as tk
from tkinter import ttk

import numpy as np
import pandas as pd
import pandastable as pt

from tempcf.Observable import Observable


class DataView(tk.Frame):

    def __init__(self, parent):
        self.parent = parent
        self.data_view_observable = Observable()
        self.table_data = None
        self.main = tk.Frame(self.parent)
        self.table_frame = tk.Frame(self.main)
        self.button_frame = tk.Frame(self.main)
        self.select_button = tk.Button(self.button_frame, text="Select Data", state=tk.DISABLED,
                                       command=self.select_data)
        self.view_data_var = tk.IntVar()
        self.view_data_button_1 = tk.Radiobutton(self.button_frame, text="View Selected Data", state=tk.DISABLED,
                                                 command=self.view_selected, indicatoron=False, value=1,
                                                 variable=self.view_data_var)
        self.view_data_button_2 = tk.Radiobutton(self.button_frame, text="View All Data", state=tk.DISABLED,
                                                 command=self.view_all, indicatoron=False, value=2,
                                                 variable=self.view_data_var)
        self.view_data_button_2.select()
        self.undock_button = tk.Button(self.main, text="Undock", state=tk.DISABLED, command=self.undock)
        self.dock_button = None
        self.undock_button.pack(side=tk.BOTTOM, fill=tk.X)
        self.select_button.pack(side=tk.RIGHT)
        self.view_data_button_1.pack(side=tk.LEFT)
        self.view_data_button_2.pack(side=tk.LEFT)
        self.button_frame.pack(side=tk.TOP, fill=tk.X)
        self.table_frame.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=1)
        self.main.pack(side=tk.LEFT, fill=tk.Y, expand=1)
        self.table = pt.Table(self.table_frame, showtoolbar=False, showstatusbar=True, width="405", editable=False)
        self.table.show()
        return

    def load_data(self, data: pd.DataFrame):
        self.table_data = data[["time", "depth", "temperature"]].copy()
        self.table.model.df = self.table_data
        self.table.resetColors()
        self.table.autoResizeColumns()
        self.table.redraw()
        if self.view_data_var.get() == 2:
            self.select_button.config(state=tk.NORMAL)
        self.view_data_button_1.config(state=tk.NORMAL)
        self.view_data_button_2.config(state=tk.NORMAL)
        if self.undock_button is not None:
            self.undock_button.config(state=tk.NORMAL)
        if self.dock_button is not None:
            self.dock_button.config(state=tk.NORMAL)
        return

    def select_data(self):
        selected = self.table.getSelectedDataFrame()
        indices = list(selected.index)
        self.colour_rows(indices, "#36DDEE")
        self.data_view_observable.callbacks["select_data"](np.array(indices))
        return

    def colour_rows(self, indices, colour="#36DDEE"):
        self.table.resetColors()
        self.table.setRowColors(rows=indices, clr=colour, cols="all")
        self.table.redraw()
        return

    def view_selected(self):
        self.select_button.config(state=tk.DISABLED)
        self.data_view_observable.callbacks["view_selected"]()
        return

    def view_all(self):
        self.select_button.config(state=tk.NORMAL)
        self.data_view_observable.callbacks["view_all"]()
        return

    def close_table(self):
        self.table.clearTable()
        self.table_data = None
        self.select_button.config(state=tk.DISABLED)
        self.view_data_button_1.config(state=tk.DISABLED)
        self.view_data_button_2.config(state=tk.DISABLED)
        if self.undock_button is not None:
            self.undock_button.config(state=tk.DISABLED)
        if self.dock_button is not None:
            self.dock_button.config(state=tk.DISABLED)
        return

    def undock(self):
        self.main.pack_forget()
        self.data_view_observable.callbacks["undock"]()
        return

    def dock(self):
        self.data_view_observable.callbacks["dock"]()
        return

    def show_undocked(self, parent):
        self.parent = parent
        self.main = tk.Frame(self.parent)
        self.table_frame = tk.Frame(self.main)
        self.button_frame = tk.Frame(self.main)
        self.select_button = tk.Button(self.button_frame, text="Select Data", state=tk.DISABLED,
                                       command=self.select_data)

        self.view_data_button_1 = tk.Radiobutton(self.button_frame, text="View Selected Data", state=tk.NORMAL,
                                                 command=self.view_selected, indicatoron=False, value=1,
                                                 variable=self.view_data_var)
        self.view_data_button_2 = tk.Radiobutton(self.button_frame, text="View All Data", state=tk.NORMAL,
                                                 command=self.view_all, indicatoron=False, value=2,
                                                 variable=self.view_data_var)
        if self.view_data_var.get() == 2:
            self.select_button.config(state=tk.NORMAL)
            self.view_data_button_2.select()
        elif self.view_data_var.get() == 1:
            self.view_data_button_1.select()
        else:
            print(f"data_view_var not 1 or 2 ({self.view_data_var.get()})")
        self.dock_button = tk.Button(self.main, text="Dock", state=tk.NORMAL, command=self.dock)
        self.undock_button = None
        self.dock_button.pack(side=tk.BOTTOM, fill=tk.X)
        self.select_button.pack(side=tk.RIGHT)
        self.view_data_button_1.pack(side=tk.LEFT)
        self.view_data_button_2.pack(side=tk.LEFT)
        self.button_frame.pack(side=tk.TOP, fill=tk.X)
        self.table_frame.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=1)
        self.main.pack(fill=tk.BOTH, expand=1)
        self.table = pt.Table(self.table_frame, showtoolbar=False, showstatusbar=True, width="405", height="800",
                              editable=False, dataframe=self.table_data)
        self.table.show()
        return

    def show_docked(self, parent):
        self.parent = parent
        self.main = tk.Frame(self.parent)
        self.table_frame = tk.Frame(self.main)
        self.button_frame = tk.Frame(self.main)
        self.select_button = tk.Button(self.button_frame, text="Select Data", state=tk.DISABLED,
                                       command=self.select_data)

        self.view_data_button_1 = tk.Radiobutton(self.button_frame, text="View Selected Data", state=tk.NORMAL,
                                                 command=self.view_selected, indicatoron=False, value=1,
                                                 variable=self.view_data_var)
        self.view_data_button_2 = tk.Radiobutton(self.button_frame, text="View All Data", state=tk.NORMAL,
                                                 command=self.view_all, indicatoron=False, value=2,
                                                 variable=self.view_data_var)
        if self.view_data_var.get() == 2:
            self.select_button.config(state=tk.NORMAL)
            self.view_data_button_2.select()
        elif self.view_data_var.get() == 1:
            self.view_data_button_1.select()
        else:
            print(f"data_view_var not 1 or 2 ({self.view_data_var.get()})")
        self.undock_button = tk.Button(self.main, text="Undock", state=tk.NORMAL, command=self.undock)
        self.dock_button = None
        self.undock_button.pack(side=tk.BOTTOM, fill=tk.X)
        self.select_button.pack(side=tk.RIGHT)
        self.view_data_button_1.pack(side=tk.LEFT)
        self.view_data_button_2.pack(side=tk.LEFT)
        self.button_frame.pack(side=tk.TOP, fill=tk.X)
        self.table_frame.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=1)
        self.main.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
        self.table = pt.Table(self.table_frame, showtoolbar=False, showstatusbar=True, width="405", editable=False,
                              dataframe=self.table_data)
        self.table.show()
        return
