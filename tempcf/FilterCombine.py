import tkinter as tk
from tempcf.PopoutDialog import PopoutDialog


class FilterCombine(PopoutDialog):
    def __init__(self, parent):
        PopoutDialog.__init__(self, parent)
        self.dialog.geometry("500x400+250+250")
        self.dialog.title("Combine Filters")
        self.and_or_frame = tk.Frame(self.dialog)
        self.filter_type = tk.IntVar()
        self.and_switch = tk.Radiobutton(self.and_or_frame, text="AND\n(match all filters)", variable=self.filter_type, value=0,
                                         indicatoron=False, padx=5)
        self.or_switch = tk.Radiobutton(self.and_or_frame, text="OR\n(match any filter)", variable=self.filter_type, value=1,
                                        indicatoron=False, padx=5)
        self.and_switch.select()
        self.and_switch.pack(side=tk.LEFT, pady=10)
        self.or_switch.pack(side=tk.RIGHT, pady=10)
        self.check_button_frame = tk.Frame(self.dialog)
        self.check_buttons = {}
        self.check_button_values = {}
        self.filter_combine_list = []
        self.error_text = tk.Message(self.dialog, text="", font=("Arial", 14), fg="red", pady=5, width=400)
        return

    def display(self, filter_info: dict):
        title = tk.Message(self.dialog, text="Combine filters", font=("Courier New", 16), width=400)
        title.pack(side=tk.TOP, fill=tk.X, expand=0)

        for filter_id, params in filter_info.items():
            row_frame = tk.Frame(self.check_button_frame)
            text = f"{filter_id}: {params[-1][2]}\n"
            for p in params[:-1]:
                text = f"{text}\t{p[0]}: {p[2]}\n"
            text = text[:-1]
            self.check_button_values[filter_id] = tk.BooleanVar()
            self.check_buttons[filter_id] = tk.Checkbutton(row_frame, text=text, justify=tk.LEFT,
                                                           variable=self.check_button_values[filter_id])
            self.check_buttons[filter_id].pack(side=tk.LEFT, fill=tk.X)
            row_frame.pack(fill=tk.X)
        self.check_button_frame.pack()
        self.and_or_frame.pack(pady=5)
        btn = tk.Button(self.dialog, text="Apply", padx=15, command=self.set_combo_filter)
        btn.pack(pady=5)

        self.dialog.grab_set()
        self.dialog.focus_force()
        self.dialog.wait_window()
        return self.filter_combine_list, self.filter_type.get()

    def set_combo_filter(self):
        for key, value in self.check_button_values.items():
            if value.get():
                self.filter_combine_list.append(key)
        self.dialog.destroy()
        return
