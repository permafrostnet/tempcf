import tkinter as tk
import numpy as np
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg,
                                               NavigationToolbar2Tk)
from matplotlib.figure import Figure
from matplotlib.widgets import LassoSelector
from matplotlib.path import Path
from mpl_interactions import zoom_factory, panhandler

from tempcf.Observable import Observable


class GraphView(tk.Frame):
    unselectable_plots: list
    PLOT_PARAMETERS = {'unselected_unflagged': {'s': 2, 'c': "#535379"},
                       'unselected_unflagged_0': {'s': 2, 'c': "#FFA996"},
                       'unselected_unflagged_1': {'s': 2, 'c': "#FFA327"},
                       'unselected_unflagged_2': {'s': 2, 'c': "#FFD427"},
                       'unselected_unflagged_3': {'s': 2, 'c': "#8DCF01"},
                       'unselected_unflagged_4': {'s': 2, 'c': "#04B211"},
                       'unselected_unflagged_5': {'s': 2, 'c': "#41D091"},
                       'unselected_unflagged_6': {'s': 2, 'c': "#2CD6C4"},
                       'unselected_unflagged_7': {'s': 2, 'c': "#3AE3F3"},
                       'unselected_unflagged_8': {'s': 2, 'c': "#36ADFF"},
                       'unselected_unflagged_9': {'s': 2, 'c': "#7E6BEC"},
                       'unselected_unflagged_10': {'s': 2, 'c': "#AD6BEC"},
                       'unselected_unflagged_11': {'s': 2, 'c': "#E536EE"},
                       'selected_unflagged':   {'s': 4, 'c': "#0000cc"},
                       'unselected_flagged':   {'s': 2, 'c': "#a32929"},
                       'selected_flagged':     {'s': 4, 'c': "#cc0000"}}

    def __init__(self, parent):
        self.parent = parent
        self.frame = tk.Frame(self.parent)
        self.figure = None
        self.toolbar = None
        self.canvas = None
        self.zoom_control = None
        self.pan_control = None
        self.frame.pack()
        self.select_i = Observable(np.nonzero(0)[0])
        self.selectedshow = list()
        self.depth_colours = dict()

    def resetSelection(self):
        """ Set selection to no data points without triggering callbacks."""
        self.select_i.data = np.nonzero(0)[0]

    def onSelect(self, data):
        """ Update the indices of selected points.

        data --- the vertices of the path obtained from LassoSelector
        """
        path = Path(data)
        self.select_i.set(np.nonzero(path.contains_points(self.xys))[0])

    def selectFilteredData(self, indices):
        """ Select all data for which the active filter is True.

        indices --- boolean array created by a filter"""
        self.select_i.set((np.where(indices)[0], True))
        self.canvas.draw_idle()

    def createGraph(self, original, selectable, depths):
        """ Create graphing canvas from scratch and draw data
        original --- unmodified dataframe (m x n)
        selectable --- dataframe that will be edited (m x n)
        depths --- boolean array of length m corresponding to which depths are active
        """
        if self.figure:
            self.figure.clear()
        if self.toolbar:
            self.toolbar.destroy()

        self.figure = Figure(figsize=(10, 8))
        self.ax = self.figure.add_subplot(111)
        self.canvas = FigureCanvasTkAgg(self.figure, master=self.parent)
        self.drawOriginal(original, self.ax)
        self.drawSelectable(selectable, depths, self.select_i.get(), None)
        self.lasso = LassoSelector(self.ax, onselect=self.onSelect, button=1)
        self.toolbar = NavigationToolbar2Tk(self.canvas, self.parent)
        self.canvas.get_tk_widget().pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
        self.zoom_control = zoom_factory(self.ax, base_scale=1.3)
        self.pan_control = panhandler(self.figure, button=3)
        self.toolbar.update()
        self.canvas.draw()

    def closeGraph(self):
        """ Remove graph object from canvas """
        if self.canvas:
            self.canvas.get_tk_widget().destroy()
            self.resetSelection()

    def drawOriginal(self, original, depths):
        """ Add temperature time-series line graph of unmodified dataset to canvas."""
        if hasattr(self, "originalplot"):
            del self.originalplot

        # TODO: don't make groupby transformation on-the-fly
        widedata = original.groupby(['time', 'depth'])['temperature'].aggregate('mean').unstack()

        self.originalplot = self.ax.plot(widedata.index.values, widedata.values, '#5e5e5e', linewidth=0.1, zorder=1)

    def removeDataFromGraph(self):
        """ remove series objects from matplotlib canvas if they exist"""
        if hasattr(self, "selectedplot"):
            self.selectedplot.remove()

        if hasattr(self, "selectedshow"):
            while len(self.selectedshow) > 0:
                S = self.selectedshow.pop()
                S.remove() if S else None

    def drawSelectable(self, selectable, depths, select_i, filtered):
        """ Draw data points.

        Points are drawn with different colours and sizes depending on whether they are filtered or selected.

        selectable --- a (m x n) dataframe with columns (time, temperature, depth)
        depths --- boolean array of length m corresponding to which depths are toggled on
        select_i --- list of integers corresponding to the indices of the data points that have been selected.
        filtered --- boolean array of length  m identifying which indicies of the data frame have been identified by a filter
        """
        self.removeDataFromGraph()

        self.selectedplot = self.ax.scatter(selectable['time'],
                                            selectable['temperature'],
                                            s=0)  # these don't have markers but are used to get the index
        self.xys = self.selectedplot.get_offsets()

        selected = np.zeros(selectable.shape[0], dtype=bool)
        selected[select_i] = True

        if filtered is None:
            filtered = np.zeros_like(selected, dtype=bool)

        self.selectedshow = list()
        self.depth_colours = dict()

        # Adding different colours as different series is much faster than passing an array of plot properties
        depth_list = list(set(selectable["depth"]))
        depth_list.sort()
        for i, d in enumerate(depth_list):
            selectable_subset = selectable.loc[selectable["depth"] == d]
            param_key = f'unselected_unflagged_{i}'
            if param_key in self.PLOT_PARAMETERS.keys():
                self.depth_colours[d] = self.PLOT_PARAMETERS[param_key]["c"]
                self.selectedshow.append(
                    self.__drawPointsWithProperties(selectable_subset,
                                                    np.logical_not(selected[selectable_subset.index])
                                                    & np.logical_not(filtered[selectable_subset.index]),
                                                    param_key, depths[selectable_subset.index]))
            else:
                self.depth_colours[d] = self.PLOT_PARAMETERS["unselected_unflagged"]["c"]
                self.selectedshow.append(
                    self.__drawPointsWithProperties(selectable_subset,
                                                    np.logical_not(selected[selectable_subset.index])
                                                    & np.logical_not(filtered[selectable_subset.index]),
                                                    'unselected_unflagged', depths[selectable_subset.index]))
        self.selectedshow.append(self.__drawPointsWithProperties(selectable, selected & np.logical_not(filtered),
                                                                 'selected_unflagged', depths))
        self.selectedshow.append(self.__drawPointsWithProperties(selectable, np.logical_not(selected) & filtered,
                                                                 'unselected_flagged', depths))
        self.selectedshow.append(self.__drawPointsWithProperties(selectable, selected & filtered,
                                                                 'selected_flagged', depths))

        # Preserve zoom settings after making a lasso selection
        self.ax.set_xlim(self.ax.get_xlim())
        self.ax.set_ylim(self.ax.get_ylim())
        self.canvas.draw_idle()

    def __drawPointsWithProperties(self, data, selected, properties, depths):
        """ Add a points data series to the canvas.

        data --- a (m x n) dataframe with columns (time, temperature, depth)
        selected --- boolean array of length m corresponding to which indices of the data have been selected.
        properties --- dictionary of matplotlib plotting parameters
        depths --- boolean array of length m corresponding to which depths are toggled on
        """
        params = self.PLOT_PARAMETERS[properties]
        plotTrue = selected & depths
        if np.any(plotTrue):
            y = data.loc[plotTrue, 'temperature']
            if not np.all(np.isnan(y)):
                return self.ax.scatter(data.loc[plotTrue, 'time'],
                                       y, zorder=2, **params)

    def hide_graph(self):
        self.canvas.get_tk_widget().pack_forget()
        return

    def show_graph(self):
        self.canvas.get_tk_widget().pack()
        return

    def clear_unselectable(self):
        if hasattr(self, "unselectable_plots"):
            for lines, points in self.unselectable_plots:
                while len(lines) > 0:
                    l = lines.pop()
                    l.remove() if l else None
                points.remove()
            del self.unselectable_plots
            self.canvas.draw_idle()
        return

    def draw_unselectable(self, unselectable_data):
        self.clear_unselectable()
        self.unselectable_plots = list()
        for depth in unselectable_data["depth"].unique():
            if depth in self.depth_colours.keys():
                colour = self.depth_colours[depth]
            else:
                colour = self.PLOT_PARAMETERS["unselected_unflagged"]["c"]
            subset = unselectable_data.loc[unselectable_data["depth"] == depth]
            self.unselectable_plots.append((self.ax.plot(subset["time"], subset["temperature"], c=colour, linewidth=0.3,
                                                         zorder=1),
                                           self.ax.scatter(subset["time"], subset["temperature"], c="#5e5e5e", s=1,
                                                           zorder=2)))
        self.canvas.draw_idle()
        return
